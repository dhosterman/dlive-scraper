# -*- coding: utf-8 -*-
import json

import scrapy


class EarningsSpider(scrapy.Spider):
    name = "earnings"
    allowed_domains = ["dlive.tv"]
    base_url = "https://graphigo.prd.dlive.tv"

    def _body(self, username=None, after=None):
        body = {
            "operationName": "LivestreamProfileWallet",
            "variables": {"displayname": username, "first": 20, "isLoggedIn": False,},
            "extensions": {
                "persistedQuery": {
                    "version": 1,
                    "sha256Hash": "33f93541f9c5eda40a8b54c152246f953564c441361eb3b729373ae89165b798",
                },
            },
        }
        if after:
            body["variables"]["after"] = str(after)

        return json.dumps(body)

    def start_requests(self):
        for username in self.settings.get("USERNAMES"):
            body = self._body(username=username)
            yield scrapy.Request(
                url=self.base_url, callback=self.parse, method="POST", body=body
            )

    def parse(self, response):
        data = json.loads(response.body)
        displayname = data["data"]["userByDisplayName"]["displayname"]
        for transaction in data["data"]["userByDisplayName"]["transactions"]["list"]:
            seq = transaction.get("seq")
            yield transaction

        if data["data"]["userByDisplayName"]["transactionsLino"]["pageInfo"][
            "hasPreviousPage"
        ]:
            yield scrapy.Request(
                url=self.base_url,
                method="POST",
                body=self._body(username=displayname, after=seq),
            )
